######LCMS#################
df<- read.csv("P:/Giorgia Greter/Experiments/2022/GG022_016/LCMS/CC20221123GGreter_quantification_result.csv")
wgts<- read.csv("P:/Giorgia Greter/Experiments/2022/GG022_016/LCMS/CC_samples_weight.csv", sep=",")
LOB<- read.csv("P:/Giorgia Greter/Experiments/2022/GG022_016/LCMS/LOB.csv")


df<-df %>% rename("level"= "level..uM.")%>%
  separate(sample, into=c("sample","well"), sep='\\_[^\\_]*$')%>%
  filter(grepl("GG",sample))

df <- merge(df,wgts, by = "sample")

df<-df%>%mutate(level = as.numeric(level))%>%
  mutate(weight = as.numeric(weight))%>%
  mutate(level_norm =  level/ weight)

## normalise LOBs to weight. Divide by weight mean.
wgts<-wgts%>%mutate(weight = as.numeric(weight))
x<-mean(wgts[["weight"]]) ## weight mean
LOB<-LOB%>%mutate(LOB = as.numeric(LOB))%>%
  mutate(LOB_norm =  LOB/ x)


##Add LOB coloumn to df
df <- merge(df,LOB,by=c("molecule"))
df$molecule = gsub("acetate","Acetate",df$molecule)
df$molecule = gsub("butyrate","Butyrate",df$molecule)
df$molecule = gsub("propionate","Propionate",df$molecule)
df$molecule = gsub("succinate","Succinate",df$molecule)
df$molecule = gsub("lactate","Lactate",df$molecule)
## Add a sample type coloumn

df<-df%>%mutate(Type= if_else(sample=="GG22016_1"|sample=="GG22016_2"|
                                sample=="GG22016_3"|sample=="GG22016_4"|
                                sample=="GG22016_5","PBS","Lactulose"))%>%
  #mutate(level_norm=level_norm/1000)%>%mutate(LOB_norm=LOB_norm/1000)%>%
  filter(sample!="GG22016_1")%>%filter(sample!="GG22016_2")

rm(LOB,wgts,x)

## Plot
df$Type<- factor(df$Type,levels = c("PBS","Lactulose"))

p2 <-ggplot(df, aes(x=Type, y=level_norm ,fill=Type))+
  geom_boxplot()+facet_wrap(~ df$molecule ,nrow=1,scales="fixed")+
  theme_classic()+ylab("Concentration [\u00b5M/mg]")+xlab(" ")+
  geom_hline(data = df, aes(yintercept = LOB_norm),linetype="dashed")+
  ggtitle("SCFA concentration in cecum content")+
  scale_fill_brewer(palette="Set1")+
  stat_compare_means(method="t.test",label="p.format",size=4,
                     hide.ns=F)+
  theme(axis.text.x = element_text(size=10), 
        axis.text.y = element_text(size=10),
        axis.title.x = element_text(size=14),
        axis.title.y = element_text(size=14),
        plot.title = element_text(size=18),
        legend.text = element_text(size = 12))+
  theme(legend.position = "bottom")

p2


pdf("P:/Giorgia Greter/Experiments/2022/GG022_016/Plots/SCFA_levels.pdf",width=8,height=6)
grid.arrange(p2) #first page
dev.off()
